# -*- coding: utf-8 -*-
from __future__ import division
from serficQt_ui import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QInputDialog
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
import subprocess as sub

import serial.tools.list_ports
import signal
import time
import math
from datetime import datetime

class results_updated(QtCore.QObject):
	results_updated = pyqtSignal()
	def __init__(self):
		QtCore.QObject.__init__(self)
	def updated(self):
		self.results_updated.emit()
test_signal = results_updated()#TODO

class output(QtCore.QObject):
	new = pyqtSignal(str)
	def __init__(self):
		QtCore.QObject.__init__(self)
	def msg(self, str):
		self.new.emit(str)
out = output()

class sudo_check(QtCore.QObject):
	no_sudo = pyqtSignal()
	cancel = pyqtSignal()
	
	wait_for_input = False
	cancelled = False
	
	__password = ""
	
	def __init__(self):
		QtCore.QObject.__init__(self)
			
	def have_sudo(self):
		try:
			sub.check_output('sudo -nv'.split(), stderr=sub.STDOUT)
			return True
		except:
			return False

	def pass_prompt(self, mainWindow):
		__password, status = QInputDialog.getText(mainWindow, 'sudo password', 'sudo password: ', QtWidgets.QLineEdit.Password)
		if status:
			self.__password = str(__password)
			return 0
		else:
			return -1
			
	def get_sudo(self):
		self.session = self.have_sudo()
		if self.session == False:
			self.login()
			while self.session == False:
				if self.cancelled == True:
					self.cancel_login(False)
					return False
				time.sleep(0.5)
		return True
		
	@pyqtSlot()
	def start_session(self, mainWindow):
		self.cancel_login(False)
		self.wait_for_input = True
		
		if sudo.pass_prompt(mainWindow) == -1:
			out.msg("Cancelled sudo login")
			sudo.cancel_login()
			sudo.stop()
			self.wait_for_input = False
			return -1

		self.wait_for_input = False
		
		try:
			sudo_proc = sub.Popen('sudo -S echo YES'.split(), stdout=sub.PIPE, stdin=sub.PIPE, stderr=sub.PIPE)
			output, err = sudo_proc.communicate(input=self.__password+'\n')
			for line in err.split('\n'):
				substr = line.split(' ')
				if len(substr) == 7 and " ".join(line.split(' ')[4:7]) == "Sorry, try again.":
					out.msg("Wrong password, please try again")
					self.start_session(mainWindow)
					return False
				print err
			self.session = True
			out.msg("Sudo session ok")
			return True
		except:
			out.msg("EXCEPT: 001 This should not happen..")
			return False

	def cancel_login(self, status=True):
		self.cancelled = status
		
	def login(self):
		self.no_sudo.emit()
	def stop(self):
		self.cancel.emit()

sudo = sudo_check()

item_dict = {}
def add_item_to_results_tree(tree, data, label_count_value=None):
	global item_dict
	"""
	0 ID
	1 X
	2 Y
	3 BSSID
	4 SIGNAL
	5 CHANNEL
	6 ESSID
	"""
	icon1 = QtGui.QIcon()
	signal_icon = ""
	if int(data[4]) in range(-30,-37): signal_icon = ":/ico/icons/signal_30_black.png"
	elif int(data[4]) in xrange(-38,-46, -1): signal_icon = ":/ico/icons/signal_38_black.png"
	elif int(data[4]) in xrange(-46,-54, -1): signal_icon = ":/ico/icons/signal_46_black.png"
	elif int(data[4]) in xrange(-54,-62, -1): signal_icon = ":/ico/icons/signal_54_black.png"
	elif int(data[4]) in xrange(-62,-70, -1): signal_icon = ":/ico/icons/signal_62_black.png"
	elif int(data[4]) in xrange(-70,-78, -1): signal_icon = ":/ico/icons/signal_70_black.png"
	elif int(data[4]) in xrange(-78,-85, -1): signal_icon = ":/ico/icons/signal_78_black.png"
	elif int(data[4]) in xrange(-85,-100, -1): signal_icon = ":/ico/icons/signal_86_black.png"

	icon1.addPixmap(QtGui.QPixmap(signal_icon), QtGui.QIcon.Normal, QtGui.QIcon.On)
	
	key = data[3].strip(':')
	
	if not tree.topLevelItemCount(): #first item
		item_dict[key] = int(data[0]) - 1 # index 0 'cause it's the first item.
		
		item_0 = QtWidgets.QTreeWidgetItem(tree)
		item_0.setIcon(0, icon1)
		item_0.setText(0, "{}".format(data[4]))
		item_0.setText(1, "{:3}".format(data[1]))
		item_0.setText(2, "{:3}".format(data[2]))
		item_0.setText(3, "{}".format(data[5]))
		item_0.setText(4, "{}".format(data[3]))
		item_0.setText(5, "{}".format(data[6]))
		
		# align to right
		item_0.setTextAlignment(1, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		item_0.setTextAlignment(2, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		item_0.setTextAlignment(3, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		
		#tree.resizeColumnToContents(0)
		tree.setColumnWidth(0,70)
		tree.setColumnWidth(1,50)
		tree.setColumnWidth(2,50)
		tree.setColumnWidth(3,50)
		tree.setColumnWidth(4,160)

	elif key in item_dict: # update item (actually, update position)
		if tree.topLevelItemCount() >= item_dict[key]: # check if index exists
			tree.topLevelItem( item_dict[key] ).setIcon(0, icon1)
			tree.topLevelItem( item_dict[key] ).setText(0, data[4])
			tree.topLevelItem( item_dict[key] ).setText(1, "{:3}".format(data[1]))
			tree.topLevelItem( item_dict[key] ).setText(2, "{:3}".format(data[2]))
			tree.topLevelItem( item_dict[key] ).setText(3, data[5])
			tree.topLevelItem( item_dict[key] ).setText(4, data[3])
			tree.topLevelItem( item_dict[key] ).setText(5, data[6])
		else:
			out.msg("wtf")
	else: # new item
		item_dict[key] = int(data[0]) - 1
		item_0 = QtWidgets.QTreeWidgetItem(tree)
		item_0.setIcon(0, icon1)
		item_0.setText(0, "{}".format(data[4]))
		item_0.setText(1, "{:3}".format(data[1]))
		item_0.setText(2, "{:3}".format(data[2]))
		item_0.setText(3, "{}".format(data[5]))
		item_0.setText(4, "{}".format(data[3]))
		item_0.setText(5, "{}".format(data[6]))
		
		# align to right
		item_0.setTextAlignment(1, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		item_0.setTextAlignment(2, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		item_0.setTextAlignment(3, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)

class new_scan_thread(QThread):
	def __init__(self, tree_widget, args, progressbar):
		QThread.__init__(self)
		self.args = args
		self.tree_widget = tree_widget
		self.progressbar = progressbar
		
		self.trigger_start_data = False
		self.steps_todo = 0;
		self.steps_done = 0;
		self.percent = 0;
		self.arduino_path = "/dev/ttyUSB0"
		self.wifi_interface = "wlan0"

	def __del__(self):
		self.wait()
		
	def scan(self):
		data = ""
		cmd = 'sudo ../serfic -h0,10,10 -v50,50,10 -s3 -V1 -f%COUNT%,%HORIZONTAL_ANGLE%,%VERTICAL_ANGLE%,%BSSID%,%SIGNAL%,%CHANNEL%,%ESSID% -a/dev/ttyUSB0 wlan0'.split()
		cmd[2] = "-h{},{},{}".format(self.args[0],self.args[1],self.args[2])
		cmd[3] = "-v{},{},{}".format(self.args[3],self.args[4],self.args[5])
		cmd[4] = "-s{}".format(self.args[6]);# scan amount
		cmd[7] = "-a{}".format(self.args[7]);# arduino
		cmd[8] = "{}".format(self.args[8]);# wifi interface
		
		steps_todo_x = 0;
		steps_todo_y = 0;
		if int(self.args[2]) > 0:
			steps_todo_x = math.ceil(((int(self.args[1]) - int(self.args[0])) / int(self.args[2])) +1)# +1 'cause we count from 0
		if int(self.args[5]) > 0:
			steps_todo_y = math.ceil(((int(self.args[4]) - int(self.args[3])) / int(self.args[5])) +1)

		print steps_todo_x
		print steps_todo_y
		self.steps_todo = steps_todo_x * steps_todo_y
		self.steps_done = 0

		if sudo.get_sudo() == False:
			raise StopIteration
		out.msg("sudo ok")
			
		self.proc = sub.Popen(cmd, bufsize=-1, stdout=sub.PIPE)
		print self.proc.pid
		for line in iter(self.proc.stdout.readline,b''):
			if line == '':
				time.sleep(0.5)
			#if line.rstrip() == "Ok start scanning.":
			if not self.trigger_start_data and line.rstrip()[:6] == "CURPOS":
				# parse X and Y
				# .. TODO
				print "Current position {}".format(line.rstrip()[6:])
				self.trigger_start_data = True
				continue; # next line
			if self.trigger_start_data:
				if line == "\n":
					self.steps_done+=1
					if self.steps_todo != 0:
						self.percent = (self.steps_done/self.steps_todo) * 100
					else:
						self.percent = 100;
					self.progressbar.setProperty("value", self.percent)
					print "{0:.0f}% done".format( self.percent )
					print self.steps_todo
					print self.steps_done
					yield data
					data = ""
					self.trigger_start_data = False
					test_signal.updated() # update counter
					continue;
				data += line
			else:
				out.msg("serfic: {}".format(line.rstrip()))
		
		self.proc.stdout.close()

	def stop(self):
		if sudo.get_sudo() == False:
			out.msg("cannot get sudo, thus cannot stop.")
			return False
		else:
			out.msg("sudo ok")
		out.msg('Kill serfic.')
		print '{}'.format(self.proc.pid)
		
		if self.proc.pid > 0:
			st = sub.check_call(['sudo', 'pkill', 'serfic'])
			print "xxx" + str(st)
			#sub.check_call(['sudo', 'kill', '{}'.format(str(self.proc.pid))])
		self.terminate()
		self.wait()
		return True


	def run(self):
		print 'New scan.'
		out.msg('New scan.')
		for data_block in self.scan():
			lines = data_block.split("\n")
			for line in lines:
				data = line.split(",", 7)
				if len(data) == 7:
					print "{} {} {} {} {} {} {}".format(data[0],data[1],data[2],data[3],data[4],data[5],data[6])
					"""
					0 ID
					1 X
					2 Y
					3 BSSID
					4 SIGNAL
					5 CHANNEL
					6 ESSID
					"""
					add_item_to_results_tree(self.tree_widget, data)
			self.tree_widget.sortItems(0, 0)# 0 asc, 1 desc
# END THREAD CLASS

def scan_done(ui):
	print "yes done"
	out.msg("Done.")
	ui.progressBar.setEnabled(False)
	ui.btn_new_scan.setStyleSheet("background-color: rgb(137, 180, 45);")
	ui.btn_new_scan.setText("NEW SCAN");
	
	#enable settings again
	ui.y_tics.setEnabled(True)
	ui.x_tics.setEnabled(True)
	ui.scan_amount.setEnabled(True)
	ui.slider_x_min.setEnabled(True)
	ui.slider_x_max.setEnabled(True)
	ui.slider_y_min.setEnabled(True)
	ui.slider_y_max.setEnabled(True)
	ui.comboBox_wif.setEnabled(True)
	ui.comboBox_arduino.setEnabled(True)
	
	ui.menu_serfic_load_settings.setEnabled(True)
	ui.menu_serfic_save_settings.setEnabled(True)
	ui.menu_serfic_load_results.setEnabled(True)
	ui.menu_serfic_save_results.setEnabled(True)
	ui.menu_serfic_exit.setEnabled(True)
	
	try:
		del ui.scan_thread
	except:
		print("No thread yet.. so nothing to kill");

def new_scan(ui):#button
	try:
		ui.scan_thread
	except:
		arduino_path = ui.comboBox_arduino.currentText()
		wifi_interface = ui.comboBox_wif.currentText()
		if arduino_path == "":
			out.msg("Please select a path to you arduino device first.")
			return -1
		if wifi_interface == "":
			out.msg("Please select a wifi interface first.")
			return -1
		
		ui.treeWidget.clear()
		global item_dict
		item_dict = {}
		ui.progressBar.setProperty("value", 0)
		ui.progressBar.setEnabled(True)
		
		#lock settings
		ui.y_tics.setEnabled(False)
		ui.x_tics.setEnabled(False)
		ui.scan_amount.setEnabled(False)
		ui.slider_x_min.setEnabled(False)
		ui.slider_x_max.setEnabled(False)
		ui.slider_y_min.setEnabled(False)
		ui.slider_y_max.setEnabled(False)
		ui.comboBox_wif.setEnabled(False)
		ui.comboBox_arduino.setEnabled(False)
		
		ui.menu_serfic_load_settings.setEnabled(False)
		ui.menu_serfic_save_settings.setEnabled(False)
		ui.menu_serfic_load_results.setEnabled(False)
		ui.menu_serfic_save_results.setEnabled(False)
		ui.menu_serfic_exit.setEnabled(False)
		
		# x min, x max, x steps, y min, y max, x steps, scan_amount
		# TODO steps, now static
		args = [ui.slider_x_min.value(),
				180-ui.slider_x_max.value(),
				ui.x_tics.value(),
				ui.slider_y_min.value(),
				180-ui.slider_y_max.value(),
				ui.y_tics.value(),
				ui.scan_amount.value(),
				#self.arduino_path,
				# TODO checks!
				ui.comboBox_arduino.currentText(),
				ui.comboBox_wif.currentText()
				#self.wifi_interface
			];
		
		ui.scan_thread = new_scan_thread(ui.treeWidget, args, ui.progressBar)
		ui.scan_thread.setTerminationEnabled = True
		ui.scan_thread.finished.connect(lambda: scan_done(ui))
		ui.scan_thread.start()
		
		# change NEW SCAN to STOP and make BG red
		ui.btn_new_scan.setText("STOP");
		ui.btn_new_scan.setStyleSheet("background-color: rgb(255, 56, 36);")
	else:#stop button pressed
		if ui.scan_thread.stop() == True:#terminate run() loop
			del ui.scan_thread
			ui.progressBar.setEnabled(False)
			ui.btn_new_scan.setStyleSheet("background-color: rgb(137, 180, 45);")
			ui.btn_new_scan.setText("NEW SCAN");
	

# maybe do single function to handle pairs.
def slider_value_change_min(slider, counter_value, label, steps):
	value = slider.value()
	if (value + counter_value + steps.value()) <= 180:
		label.setText("{:3}°".format(value))
	else:
		slider.setProperty("value", 180-counter_value - steps.value())
		
def slider_value_change_max(slider, counter_value, label, steps):
	value = slider.value()
	prev_value = label.text()[:3]
	if (value + counter_value+steps.value()) <= 180:
		label.setText("{:3}°".format(180-value))
	else:
		slider.setProperty("value", 180-counter_value - steps.value())

# write conf
def write_conf(settings,ui):
	settings[settings['default']]['x_min'] = ui.slider_x_min.value()
	settings[settings['default']]['x_max'] = 180-ui.slider_x_max.value()
	settings[settings['default']]['y_min'] = ui.slider_y_min.value()
	settings[settings['default']]['y_max'] = 180-ui.slider_y_max.value()
	settings[settings['default']]['scan_amount'] = ui.scan_amount.value()
	settings[settings['default']]['x_steps'] = ui.x_tics.value()
	settings[settings['default']]['y_steps'] = ui.y_tics.value()
	settings[settings['default']]['arduino_path'] = ui.comboBox_arduino.currentText()
	settings[settings['default']]['wifi_interface'] = ui.comboBox_wif.currentText()
	
	fd = open("serficQt.conf", "w")
	
	default = ""
	for _set in settings:
		if repr(_set).strip('\'') == '__builtins__':
			continue
		if repr(_set).strip('\'') == 'default':
			default = "{}={}".format(repr(_set).strip('\''), repr(settings[_set]))
			continue
		fd.write("{}={}\n".format(repr(_set).strip('\''), repr(settings[_set])))
	print default
	fd.write("{}\n".format(default))
	fd.close()
	out.msg("Saved settings {}".format(default.split("=")[1]))

def write_results(mainWindow,tree):
	filename = QFileDialog.getSaveFileName(mainWindow, 'Save results', './results', '.csv')
	if filename[0] != '':
		root_item = tree.invisibleRootItem()
		if root_item:
			fd = open(filename[0], "w")
			print "yess"
			for i in range(root_item.childCount()):
				fd.write("{},{},{},{},{},'{}'\n".format(
					root_item.child(i).text(1),
					root_item.child(i).text(2),
					root_item.child(i).text(0),
					root_item.child(i).text(3),
					root_item.child(i).text(4),
					root_item.child(i).text(5)))
			fd.close()

def read_results(mainWindow,tree):
	filename = QFileDialog.getOpenFileName(mainWindow, 'Open results', './results')
	if filename[0] != '':
		import csv
		with open(filename[0], 'rb') as file:
			reader = csv.reader(file, delimiter=",")
			tree.clear() # empty current tree
			id_count = 1;
			try:
				for x,y,signal,channel,bssid,essid in reader:
					# TODO check if values are valid
					add_item_to_results_tree(tree, [id_count,x,y,bssid,signal,channel,essid])
					id_count += 1
				out.msg("Loaded {}".format(filename[0]))
			except:
				print "result file corrupt"
				out.msg('Result file corrupt.')

def move_servo(tree,ui):
	try:
		ui.scan_thread
	except:
		item = tree.currentItem()
		print "move_servo: {} {}\t{}".format(item.text(1), item.text(2), item.text(5))
		out.msg("move_servo: {} {}\t{}".format(item.text(1), item.text(2), item.text(5)))
		sub.check_call(['../serfic', '-mx,{}'.format(item.text(1))])
		sub.check_call(['../serfic', '-my,{}'.format(item.text(2))])

@pyqtSlot()
def update_count(tree,label):
	label.setText(str(tree.topLevelItemCount()))

@pyqtSlot()
def msg(out,msg):
	out.setText('[' + str(datetime.now())[:19] + ']  ' + msg + '\n' + out.toPlainText())

def sort_column(tree, index):
	self.tree_widget.sortItems(0, 0)# 0 asc, 1 desc

if __name__ == "__main__":
	import sys
	app = QtWidgets.QApplication(sys.argv)
	MainWindow = QtWidgets.QMainWindow()
	ui = Ui_MainWindow()
	ui.setupUi(MainWindow)
    
    # read conf
	settings = {}
	try:
		execfile("serficQt.conf", settings)
	except:
		# or just create it ;-)
		print("No config, exiting..")
		exit(2)

	# load conf
	print settings[settings['default']]['wifi_interface']
	print settings[settings['default']]['arduino_path']

	# set sliders
	ui.slider_x_min.setProperty("value", settings[settings['default']]['x_min'])
	ui.slider_x_max.setProperty("value", 180-settings[settings['default']]['x_max'])
	ui.slider_y_min.setProperty("value", settings[settings['default']]['y_min'])	
	ui.slider_y_max.setProperty("value", 180-settings[settings['default']]['y_max'])

	# set labels
	ui.lbl_x_min.setText("{:3}°".format(settings[settings['default']]['x_min']))
	ui.lbl_x_max.setText("{:3}°".format(settings[settings['default']]['x_max']))
	ui.lbl_y_min.setText("{:3}°".format(settings[settings['default']]['y_min']))
	ui.lbl_y_max.setText("{:3}°".format(settings[settings['default']]['y_max']))
	
	# set comboboxes
	ui.scan_amount.setProperty("value", settings[settings['default']]['scan_amount'])
	ui.x_tics.setProperty("value", settings[settings['default']]['x_steps'])
	ui.y_tics.setProperty("value", settings[settings['default']]['y_steps'])
	
	ui.wifi_interface = settings[settings['default']]['wifi_interface']
	import netifaces
	for iface in netifaces.interfaces():# would be nice to check if interface is wireless
		ui.comboBox_wif.addItem(iface)
	ui.comboBox_wif.setCurrentIndex(
		ui.comboBox_wif.findText(settings[settings['default']]['wifi_interface'], QtCore.Qt.MatchFixedString)
	)
	
	ui.arduino_path = settings[settings['default']]['arduino_path']
	ports = list(serial.tools.list_ports.comports())
	for port in ports:
		ui.comboBox_arduino.addItem("{}".format(port[0]))

	ui.comboBox_arduino.setCurrentIndex(
		ui.comboBox_arduino.findText(settings[settings['default']]['arduino_path'], QtCore.Qt.MatchFixedString)
	)

	"""
	SHORTCUTS
	"""
	ui.menu_serfic_save_settings.setShortcut('Ctrl+P')
	ui.menu_serfic_load_settings.setShortcut('Ctrl+L')
	ui.menu_serfic_save_results.setShortcut('Ctrl+S')
	ui.menu_serfic_load_results.setShortcut('Ctrl+O')
	ui.menu_serfic_exit.setShortcut('Ctrl+Q')

	
	"""
	HOOKS
	"""
	# buttons
	ui.btn_new_scan.clicked.connect(lambda: new_scan(ui))
	
	# sliders
	ui.slider_x_min.valueChanged.connect(lambda: slider_value_change_min(ui.slider_x_min, ui.slider_x_max.value(), ui.lbl_x_min, ui.x_tics))
	ui.slider_x_max.valueChanged.connect(lambda: slider_value_change_max(ui.slider_x_max, ui.slider_x_min.value(), ui.lbl_x_max, ui.x_tics))
	ui.slider_x_max.valueChanged.connect(lambda: slider_value_change_max(ui.slider_x_max, ui.slider_x_min.value(), ui.lbl_x_max, ui.x_tics))
	ui.slider_y_min.valueChanged.connect(lambda: slider_value_change_min(ui.slider_y_min, ui.slider_y_max.value(), ui.lbl_y_min, ui.y_tics))
	ui.slider_y_max.valueChanged.connect(lambda: slider_value_change_max(ui.slider_y_max, ui.slider_y_min.value(), ui.lbl_y_max, ui.y_tics))
	
	# menu items
	ui.menu_serfic_save_settings.triggered.connect(lambda: write_conf(settings, ui))
	ui.menu_serfic_save_results.triggered.connect(lambda: write_results(MainWindow,ui.treeWidget))
	ui.menu_serfic_load_results.triggered.connect(lambda: read_results(MainWindow,ui.treeWidget))
	ui.menu_serfic_exit.triggered.connect(MainWindow.close)
	
	# results
	ui.treeWidget.doubleClicked.connect(lambda: move_servo(ui.treeWidget,ui))
		
	# test	
	test_signal.results_updated.connect(lambda: update_count(ui.treeWidget, ui.lbl_count_value))
	out.new.connect(lambda str: msg(ui.output_text, str))
	sudo.no_sudo.connect(lambda: sudo.start_session(MainWindow))
	sudo.cancel.connect(sudo.cancel_login)

	MainWindow.show()
	sys.exit(app.exec_())
